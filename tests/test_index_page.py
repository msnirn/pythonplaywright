import array
import time

import pytest
from playwright.sync_api import expect

import pages


class TestPage:

    testValues = ["123", "!@", "111 111", "qwert", "digits33qwertyuiopasdfghjklzxcvbnm",
                     "111qwert", "<body><script>document.write(location.href);</script></body>", "ИмяФамилия",
                     "1,2", "1.2"]

    def test_field_is_empty_error_massage_all_fields(self, page):
        pages.index_page.open_index_page(page)
        pages.index_page.press_continue_button(page)
        expect(page.get_by_text("Поле не заполнено").first).to_be_visible()
        expect(page.get_by_text("Поле не заполнено").nth(1)).to_be_visible()
        expect(page.get_by_text("Поле не заполнено").nth(2)).to_be_visible()

    @pytest.mark.parametrize("testValues", testValues)
    def test_name_error_massage_is_visible(self, page, testValues):
        pages.index_page.open_index_page(page)
        pages.index_page.fill_name_field(page, "digits33qwertyuiopasdfghjklzxcvbnm")
        expect(page.get_by_text("Превышен лимит символов: 32 максимум")).to_be_visible()
        pages.index_page.fill_name_field(page, testValues)
        pages.index_page.press_continue_button(page)
        pages.index_page.press_continue_button(page)
        expect(page.get_by_text("Допустимые символы (от 6 до 32): a-z, 0-9, _. Имя должно начинаться с буквы"))\
            .to_be_visible()
        expect(page.get_by_text("Поле не заполнено").first).to_be_visible()
        expect(page.get_by_text("Поле не заполнено").nth(1)).to_be_visible()
        time.sleep(2)

    @pytest.mark.parametrize("testValues", testValues)
    def test_email_error_massage_is_visible(self, page, testValues):
        pages.index_page.open_index_page(page)
        pages.index_page.fill_email_field(page, testValues)
        pages.index_page.press_continue_button(page)
        expect(page.get_by_text("Поле не заполнено").first).to_be_visible()
        expect(page.get_by_text("Формат e-mail: username@test.ru")).to_be_visible()
        expect(page.get_by_text("Поле не заполнено").nth(1)).to_be_visible()
        time.sleep(2)

    def test_password_error_massage_is_visible(self, page):
        pages.index_page.open_index_page(page)
        pages.index_page.fill_password_field(page, "12345")
        expect(page.get_by_text("Сложность пароля: слишком короткий")).to_be_visible()
        pages.index_page.press_continue_button(page)
        expect(page.get_by_text("Поле не заполнено").first).to_be_visible()
        expect(page.get_by_text("Поле не заполнено").nth(1)).to_be_visible()
        expect(page.get_by_text("Пароль должен содержать минимум 8 символов")).to_be_visible()
        pages.index_page.fill_password_field(page, "12345678")
        expect(page.get_by_text("Сложность пароля: низкая")).to_be_visible()
        pages.index_page.press_continue_button(page)
        pages.index_page.fill_password_field(page, "12345678aA")
        expect(page.get_by_text("Сложность пароля: средняя")).to_be_visible()
        pages.index_page.press_continue_button(page)
        pages.index_page.fill_password_field(page, "1qazxsw2!!slfsA")
        expect(page.get_by_text("Сложность пароля: высокая")).to_be_visible()
        pages.index_page.press_continue_button(page)
        time.sleep(2)

    def test_referral_code_error_massage_is_visible(self, page):
        pages.index_page.open_index_page(page)
        pages.index_page.fill_referral_code_field(page, "123")
        expect(page.get_by_text("Неверный формат ссылки")).to_be_visible()
        pages.index_page.fill_referral_code_field(page, "123456789")
        expect(page.get_by_text("Неверный формат ссылки")).to_be_visible()


    # def test_checkbox(self, page):
    #     pages.index_page.open_index_page(page)
    #     expect(page.locator("#input-177")).to_have_attribute("aria-checked", "false")
    #     pages.index_page.check_the_box(page)
    #     expect(page.locator("#input-177")).to_have_attribute("aria-checked", "true")
    #     time.sleep(2)






