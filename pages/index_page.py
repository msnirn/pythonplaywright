from playwright.sync_api import Page
import config

class IndexPage:
    def open_index_page(self, page: Page) -> None:
        page.goto(config.url.DOMAIN)

    def fill_name_field(self, page: Page, name=str) -> None:
        page.get_by_label("Имя пользователя").fill(name)

    def fill_email_field(self, page: Page, email=str) -> None:
        page.get_by_label("Электронная почта").fill(email)

    def fill_password_field(self, page: Page, password=str) -> None:
        page.get_by_label("Пароль").fill(password)

    def fill_referral_code_field(self, page: Page, code=str) -> None:
        page.get_by_label("Реферальный код").fill(code)

    def press_continue_button(self, page: Page) -> None:
        page.get_by_role("button", name="Далее").click()

    def check_the_box(self, page: Page) -> None:
        page.locator("#input-177").hover()
        page.locator("#input-177").click()



