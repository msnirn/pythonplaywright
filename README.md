python + playwright

Для запуска тестов нужен установленый python3.
В консоли активируем окружение activate.bat,
далее устанавливаем зависимости командой 
"pip install -r requirements.txt".
Далее команда "playwright install".
Запуск тестов командой "pytest ." в корне папки проекта. 